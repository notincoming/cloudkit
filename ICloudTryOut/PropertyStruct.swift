//
//  PropertyStruct.swift
//  ICloudTryOut
//
//  Created by Macstorm on 21.09.2016.
//  Copyright © 2016 spot. All rights reserved.
//

import Foundation

struct PropertyStruct{
    
    var name: String!
    var dependencies: [DependencyStruct]?
    var baseValue: Int!
    
    init(name: String!, baseValue: Int!){
        
        self.name = name
        self.baseValue = baseValue
    }
}