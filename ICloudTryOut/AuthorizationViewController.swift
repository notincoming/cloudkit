//
//  AuthorizationViewController.swift
//  ICloudTryOut
//
//  Created by Macstorm on 21.09.2016.
//  Copyright © 2016 spot. All rights reserved.
//

import UIKit
import ReactiveCocoa
import CloudKit

class AuthorizationViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var userRecord: CKRecord?
    var userPasswords: [CKRecord]?
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.getUserID()
       // self.getUserPasswords()
        
        self.reactTrace()
        self.registerButton.enabled = false
        self.loginButton.enabled = false
    
    }
    
    func getUserPasswords(){
        
        let query = CKQuery(recordType: "Passwords", predicate: NSPredicate(format: "TRUEPREDICATE"))
        
            self.userPasswords = [CKRecord]()
        
            self.delegate.privateDB.performQuery(query, inZoneWithID: nil, completionHandler: { (records: [CKRecord]?, error: NSError?) in
            
                if error == nil{
                    
                    guard records?.count != 0 else{
                        
                        print("No passwords")
                        print("No accounts created")
                        return
                    }
                    
                    
                    
                    for object in records!{
                        
                        self.userPasswords?.append(object)
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), {
                    
                            self.loginButton.enabled = true
                    })
                    
                    
                }else{
                    
                    print("Error at AuthorizationViewController -> getUserPasswords")
                }
        })
    }
    func getUserID(){
        
        self.delegate.defaultDB.fetchUserRecordIDWithCompletionHandler { (recordID: CKRecordID?, error: NSError?) in
        
            if error == nil{
                
                guard recordID != nil else{
                    
                    print("Error -> NO RECORD ID")
                    return
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                
                    
                    self.delegate.privateDB.fetchRecordWithID(recordID!, completionHandler: { (record: CKRecord?, error: NSError?) in
                        
                        if error == nil{
                        
                            guard record != nil else{
                                
                                print("Error -> NO RECORD")
                                return
                            }
                            
                            self.userRecord = record!
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                
                                self.registerButton.enabled = true
                                self.getUserPasswords()
                                
                            })
                            
                            
                        }else{
                            
                            print("Error at AuthorizationViewController -> getUserID -> record")
                            print(error?.description)
                        }
                        
                    })
                })
                
            }else{
                
                print("Error at AuthorizationViewController -> getUserID")
                print(error?.description)
            }
        }
    }

    func reactTrace(){
        
        self.registerButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside).subscribeNext({
        
            _ in
            
            self.registerAtCloudKit()
            
        })
        
        self.loginButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside).subscribeNext({
        
            _ in
            
            self.logInAtCloudKit()
        })
    }
    
    func logInAtCloudKit(){
        
        var indicator = false
        var passwordRecord: CKRecord?
        
        for object in self.userPasswords!{
            
            if self.passwordTextField.text! == (object.objectForKey("password") as! String){
                
                indicator = true
                passwordRecord = object
                
                break
            }
        }
        
        guard indicator else{
            
            print("Wrong password")
            return
        }
        
        let query = CKQuery(recordType: "Accounts", predicate: NSPredicate(format: "user == %@ && name == %@ && password == %@", self.userRecord!, self.loginTextField.text!, passwordRecord!))
        
        self.delegate.publicDB.performQuery(query, inZoneWithID: nil) { (records: [CKRecord]?, error: NSError?) in
            
            if error == nil{
                
                if records?.count == 0{
                    
                    print("No record in database")
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                            self.delegate.accountRecord = records?.first!
                        
                            self.performSegueWithIdentifier("MenuSegue", sender: self)
                    })
                   
                    
                }
            }else{
                
                print("Error -> logInAtCloudKit")
                print(error?.description)
            }
        }
        
    }
    func registerAtCloudKit(){
        
        guard self.userRecord != nil else{
            
            print("Error -> registerAtCloudKit -> no user record")
            return
        }
        
        let query = CKQuery(recordType: "Accounts", predicate: NSPredicate(format: "TRUEPREDICATE"))
        
        self.delegate.publicDB.performQuery(query, inZoneWithID: nil) { (records: [CKRecord]?, error: NSError?) in
            
            if error == nil{
                
                guard records != nil else{
                    
                    print("Error at AuthorizationViewController -> registerAtCK -> truepredicate query NIL RECORDS")
                    return
                }
            
                if records?.count != 0{
                    
                    var indicator = false
                    
                    for object in records!{
                        
                        let name = object.objectForKey("name") as! String
                        
                        if self.loginTextField.text! == name{
                            
                            indicator = true
                            break
                        }
                    }
                    
                    guard !indicator else{
                        
                        print("That name already exists")
                        return
                    }
                    
                    self.processRegistration()
                    
                }else{
                    
                    self.processRegistration()
                }
            }else{
                
                print("Error at AuthorizationViewController -> registerAtCK -> truepredicate query")
            }
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func processRegistration(){
        
        let passwordRecord = CKRecord(recordType: "Passwords")
       
        // let userReference = CKReference(record: self.userRecord!, action: .DeleteSelf)
        
        passwordRecord.setObject(self.passwordTextField.text!, forKey: "password")
        self.delegate.privateDB.saveRecord(passwordRecord) { (record: CKRecord?, error: NSError?) in
            
            if error == nil{
                
                guard record != nil else{
                    
                    print("Error at AuthorizationViewController -> processRegistration -> pass save NIL RECORD")
                    return
                }
                
                print("Password saved!")
                
                self.userPasswords?.append(record!)
                
                let accountRecord = CKRecord(recordType: "Accounts")
                let userReference = CKReference(record: self.userRecord!, action: .DeleteSelf)
                let passwordReference = CKReference(record: record!, action: .DeleteSelf)
                
                accountRecord.setObject(self.loginTextField.text!, forKey: "name")
                accountRecord.setObject(userReference, forKey: "user")
                accountRecord.setObject(passwordReference, forKey: "password")
                
                self.delegate.publicDB.saveRecord(accountRecord, completionHandler: { (record: CKRecord?, error: NSError?) in
                    
                    if error == nil{
                        
                        guard record != nil else{
                            
                            print("Error at AuthorizationViewController -> processRegistration -> account save NIL RECORD")
                            return
                        }
                        
                        print("Account saved!")
                        
                    }else{
                        
                        print("Error at AuthorizationViewController -> processRegistration -> account save")
                    }
                })
            }else{
                
                print("Error at AuthorizationViewController -> processRegistration -> pass save")
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
