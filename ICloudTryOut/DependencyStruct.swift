//
//  DependencyStruct.swift
//  ICloudTryOut
//
//  Created by Macstorm on 21.09.2016.
//  Copyright © 2016 spot. All rights reserved.
//

import Foundation

struct DependencyStruct{
    
    var name: String!
    var dependencyTable: DependencyTableStruct!
    var value: Int!
    var equationType: String!
    
    init(name: String!, dependencyTable: DependencyTableStruct!, value: Int!, equationType: String!){
        
        self.name = name
        self.dependencyTable = dependencyTable
        self.value = value
        self.equationType = equationType
    }
}