//
//  MenuViewController.swift
//  ICloudTryOut
//
//  Created by Macstorm on 21.09.2016.
//  Copyright © 2016 spot. All rights reserved.
//

import UIKit
import CloudKit
import ReactiveCocoa

class MenuViewController: UIViewController {
    
    let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    let publicDB = CKContainer.defaultContainer().publicCloudDatabase
    let privateDB = CKContainer.defaultContainer().privateCloudDatabase
    let defaultDB = CKContainer.defaultContainer()

    
    var dependencyTable = DependencyTableStruct(key: "mainTable")
    
    var adventurerCard: AdventurerCardStruct!
    
    var game: GameStruct!
    
    struct DependencyTableRecord{
        
        var key: String!
        var record: CKRecord!
        
        init(key: String!, record: CKRecord!){
            
            self.key = key
            self.record = record
        }
    }
    struct InternalCounter{
        
        static var counter = MutableProperty<Int>(0)
        static var signal = counter.signal
    }
    
    var dependencyTableRecordTable = [DependencyTableRecord]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.internalInsertion()
       // self.dependencyTablesRegistration(self.dependencyTable, previousKey: nil)
        
       // self.getDependencyTablesWithHierarchy()
    
        
        self.gameRegistration()
        
    }
    
    func getDependencyTablesWithHierarchy(){
        
        
    }
    
    func internalInsertion(){
        
        dependencyTable.container["mainTable"]?.append(DependencyTableStruct(key: "class"))
        dependencyTable.container["mainTable"]?.append(DependencyTableStruct(key: "breed"))
        

        
        dependencyTable.container["mainTable"]?[0].container["class"]?.append(DependencyTableStruct(key: "knight"))
        dependencyTable.container["mainTable"]?[0].container["class"]?.append(DependencyTableStruct(key: "mage"))
        
        dependencyTable.container["mainTable"]?[1].container["breed"]?.append(DependencyTableStruct(key: "elf"))
        dependencyTable.container["mainTable"]?[1].container["breed"]?.append(DependencyTableStruct(key: "human"))
        
        var property1 = PropertyStruct(name: "health", baseValue: 10)
        var property2 = PropertyStruct(name: "manaPoints", baseValue: 12)
        
        
        let dependency1 = DependencyStruct(name: "knight", dependencyTable: dependencyTable.container["mainTable"]?[0].container["class"]?[0], value: 10, equationType: "addition")
    
        let dependency2 = DependencyStruct(name: "class", dependencyTable: dependencyTable.container["mainTable"]?[0], value: 5, equationType: "multiplication")
        
        property1.dependencies = [dependency1, dependency2]
        property2.dependencies = [dependency1]
        
        adventurerCard = AdventurerCardStruct(properties: [property1, property2], name: "ADVCard1")
        adventurerCard.dependencyTableStruct = dependencyTable
        
        game = GameStruct(adventurerCardStruct: adventurerCard, name: "gameName", password: "gamePassword")
        

    }
    
    func gameRegistration(){
        
        let record = CKRecord(recordType: "Games")
        record.setObject(self.game.password, forKey: "password")
        record.setObject(self.game.name, forKey: "name")
        
        let accountReference = CKReference(record: self.delegate.accountRecord, action: .DeleteSelf)
        record.setObject(accountReference, forKey: "account")
        
        self.delegate.publicDB.saveRecord(record) { (record: CKRecord?, error: NSError?) in
            
            if error == nil{
                
                guard record != nil else{
                    
                    print("Error at MenuViewController -> gameRegistration NIL RECORD")
                    return
                }
                
                print("Game saved!")
                
                self.adventurerCardRegistration(record!)
                
            }else{
                
                print("Error at MenuViewController -> gameRegistration")
                print(error?.description)
            }
        }
        
    }
    
    func adventurerCardRegistration(game: CKRecord!){
        
        let record = CKRecord(recordType: "AdventurerCards")
        record.setObject(self.adventurerCard.name, forKey: "name")
        
        let gameReference = CKReference(record: game, action: .DeleteSelf)
        record.setObject(gameReference, forKey: "game")
        
        self.delegate.publicDB.saveRecord(record) { [unowned self] (record: CKRecord?, error: NSError?) in
            
     
            
            if error == nil{
                
                guard record != nil else{
                    
                    print("Error at MenuViewController -> adventurerCardRegistration NIL RECORD")
                    return
                }
                
                print("AdventurerCard saved!")
                
                    
                self.dependencyTablesRegistration(self.adventurerCard.dependencyTableStruct, previousKey: nil, adventurerCard: record!)
                
                InternalCounter.signal.observeNext({
                    
                    result in
                    
                    if result == 0{
                        
                        self.propertiesRegistration(record!)
                    }
                })
                
            }else{
                
                print("Error at MenuViewController -> adventurerCardRegistration")
            }
        }
        
    }
    func propertiesRegistration(adventurerCard: CKRecord!){
        
 
        for object in self.adventurerCard.properties{
            
            let record = CKRecord(recordType: "Properties")
            record.setObject(object.baseValue, forKey: "baseValue")
            record.setObject(object.name, forKey: "name")
            
            let adventurereCardReference = CKReference(record: adventurerCard, action: .DeleteSelf)
            record.setObject(adventurereCardReference, forKey: "adventurerCard")
            
            self.delegate.publicDB.saveRecord(record, completionHandler: { (record: CKRecord?, error: NSError?) in
                
                if error == nil{
                    
                    guard record != nil else{
                        
                        print("Error at MenuViewController -> propertiesRegistration NIL RECORD")
                        return
                    }
                    
                    print("Property saved")
                    
                    for innerObject in object.dependencies!{
                        
                        let dependencyRecord = CKRecord(recordType: "Dependencies")
                        dependencyRecord.setObject(innerObject.name, forKey: "name")
                        dependencyRecord.setObject(innerObject.equationType, forKey: "equationType")
                        dependencyRecord.setObject(innerObject.value, forKey: "value")
                        
                        let propertyReference = CKReference(record: record!, action: .DeleteSelf)
                        
                        dependencyRecord.setObject(propertyReference, forKey: "property")
                        
                        
                        var dependencyTableRecord: CKRecord?
                        
                        for dependency in self.dependencyTableRecordTable{
                            
                            if dependency.key == innerObject.name{
                                
                                dependencyTableRecord = dependency.record
                                break
                            }
                        }
                        
                        guard dependencyTableRecord != nil else{
                            
                            print("No dependency record")
                            return
                        }
                        
                        let dependencyTableReference = CKReference(record: dependencyTableRecord!, action: .DeleteSelf)
                        
                        dependencyRecord.setObject(dependencyTableReference, forKey: "dependencyTable")
                        
                        self.delegate.publicDB.saveRecord(dependencyRecord, completionHandler: { (record: CKRecord?, error: NSError?) in
                            
                            if error == nil{
                                
                                guard record != nil else{
                                    
                                    print("Error at MenuViewController -> propertiesRegistration -> dependencies NIL RECORD")
                                    return
                                }
                                
                                print("Dependency saved!")
                                
                            }else{
                                
                                print("Error at MenuViewController -> propertiesRegistration -> dependencies")
                                print(error?.description)
                            }
                        })
                    }
                    
                }else{
                    
                    print("Error at MenuViewController -> propertiesRegistration")
                }
                
               
            })
        }
        
        
    }


    
    
    func dependencyTablesRegistration(dependencyTable: DependencyTableStruct!, previousKey: CKRecord?, adventurerCard: CKRecord!){
        

        
        InternalCounter.counter.value += 1
        
        if previousKey == nil{
            

            let record = CKRecord(recordType: "DependencyTables")
            
            record.setObject(dependencyTable!.key, forKey: "name")
            record.setObject(nil, forKey: "dependencyTable")
            
            let adventurerCardReference = CKReference(record: adventurerCard, action: .DeleteSelf)
            
            record.setObject(adventurerCardReference, forKey: "adventurerCard")
            
            self.publicDB.saveRecord(record, completionHandler: { [unowned self] (record: CKRecord?, error: NSError?) in
                
                    
                if error == nil{
                    
                    print("Object saved!")
                    
                    
                    guard record != nil else{
                        
                        print("Error at MenuViewController -> depTableReg.. -> prevK == nil -> NORECORD !!")
                        return
                    }
                    
                    
                    self.dependencyTableRecordTable.append(DependencyTableRecord(key: dependencyTable!.key, record: record!))
                        
                    guard dependencyTable!.container[dependencyTable!.key]?.count != 0 else{
                        
                        return
                    }
                    
                    
                    for object in dependencyTable!.container[dependencyTable!.key]!{
                    
                        
                        self.dependencyTablesRegistration(object, previousKey: record, adventurerCard: adventurerCard)
                    }
                    
                    InternalCounter.counter.value -= 1
                
                    
                }else{
                    
                    print("Error at MenuViewController -> dependencyTableRegistration -> previousKey == nil")
                    print(error?.description)
                }
                
            })
            
            
        }else{
            
            let record = CKRecord(recordType: "DependencyTables")
            
            record.setObject(dependencyTable!.key, forKey: "name")
            
            let adventurerCardReference = CKReference(record: adventurerCard, action: .DeleteSelf)
            
            record.setObject(adventurerCardReference, forKey: "adventurerCard")
            
            let previousKeyReference = CKReference(record: previousKey!, action: .DeleteSelf)
            
            record.setObject(previousKeyReference, forKey: "dependencyTable")
            
            self.publicDB.saveRecord(record, completionHandler: { [unowned self] (record: CKRecord?, error: NSError?) in
                
                    
                if error == nil{
                    
                    print("Object saved!")
                    InternalCounter.counter.value -= 1
            
                    
                    guard record != nil else{
                        
                        print("Error -> MenuViewController -> depTabReg -> previousKey != nil -> NORECORD")
                        return
                    }
                    
                    self.dependencyTableRecordTable.append(DependencyTableRecord(key: dependencyTable!.key, record: record!))
                    
                    guard dependencyTable!.container[dependencyTable!.key]?.count != 0 else{
                        
                        return
                    }
                    
                    for object in (dependencyTable!.container[dependencyTable!.key]!){
                        
                        self.dependencyTablesRegistration(object, previousKey: record, adventurerCard: adventurerCard)
                        
                    }
                    
                 

                }else{
                    
                    print("Error at MenuViewController -> depTabReg -> previousKey != nil -> save")
                } 
                
            })
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
