//
//  GameStruct.swift
//  ICloudTryOut
//
//  Created by Macstorm on 21.09.2016.
//  Copyright © 2016 spot. All rights reserved.
//

import Foundation

struct GameStruct{
    
    var adventurerCardStruct: AdventurerCardStruct!
    var name: String!
    var password: String!
    
    init(adventurerCardStruct: AdventurerCardStruct!, name: String!, password: String!){
        
        self.adventurerCardStruct = adventurerCardStruct
        self.name = name
        self.password = password
        
    }
}