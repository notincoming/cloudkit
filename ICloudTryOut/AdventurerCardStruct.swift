//
//  AdventurerCardStruct.swift
//  ICloudTryOut
//
//  Created by Macstorm on 21.09.2016.
//  Copyright © 2016 spot. All rights reserved.
//

import Foundation

struct AdventurerCardStruct{
    
    var dependencyTableStruct: DependencyTableStruct?
    var properties: [PropertyStruct]!
    var name: String!
    
    init(properties: [PropertyStruct]!, name: String!){
        
        self.properties = properties
        self.name = name
    }
}