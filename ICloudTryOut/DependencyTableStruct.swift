//
//  DependencyTableStruct.swift
//  ICloudTryOut
//
//  Created by Macstorm on 21.09.2016.
//  Copyright © 2016 spot. All rights reserved.
//

import Foundation
import CloudKit

struct DependencyTableStruct{
    
    var key: String!
    var container = [String: [DependencyTableStruct]]()
    
    init(key: String!){
        
        self.key = key
        self.container[key] = [DependencyTableStruct]()
    }
    
}