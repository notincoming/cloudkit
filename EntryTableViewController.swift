//
//  EntryTableViewController.swift
//  ICloudTryOut
//
//  Created by Macstorm on 20.09.2016.
//  Copyright © 2016 spot. All rights reserved.
//

import UIKit
import CloudKit



class EntryTableViewController: UITableViewController {

    var listRecords = [CKRecord]()
    
    var passRecords = [CKRecord]()
    
    var records = [CKRecord]()
    
    let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.fetchUserRecordID()
        
        self.fetchRecords()
    }
    
    
    func fetchRecords(){
    
        let publicDB = CKContainer.defaultContainer().publicCloudDatabase
        let privateDB = CKContainer.defaultContainer().privateCloudDatabase
       // let reference = CKReference(record: <#T##CKRecord#>, action: <#T##CKReferenceAction#>)
        
        var query = CKQuery(recordType: "Lists", predicate: NSPredicate(format: "TRUEPREDICATE"))
        
        
            privateDB.performQuery(query, inZoneWithID: nil, completionHandler: {
               
                [unowned self]
                
                (records, error) in
                
                dispatch_async(dispatch_get_main_queue(), {
                

                    
                    if error == nil{
                        
                        print("First query done!")
                        
                        self.listRecords = records!
                        
                        query = CKQuery(recordType: "Passwords", predicate: NSPredicate(format: "password == %@", "myPassword"))
                        
                        publicDB.performQuery(query, inZoneWithID: nil, completionHandler: {
                        
                            [unowned self]
                            
                            (records, error) in
                            
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                
                                if error == nil{
                                    print("Second query done!")
                                    
                                    self.passRecords = records!
                                    
                                    let listReference = CKReference(recordID: self.listRecords[0].recordID, action: .DeleteSelf)
                                    let passReference = CKReference(recordID: self.passRecords[0].recordID, action: .DeleteSelf)
                                    
                                    self.delegate.refList = listReference
                                    self.delegate.refPass = passReference
                                    
                                    query = CKQuery(recordType: "Protected", predicate: NSPredicate(format: "list == %@ && password == %@", listReference, passReference))
                                    
                                    privateDB.performQuery(query, inZoneWithID: nil, completionHandler: {
                                    
                                        [unowned self]
                                        
                                        (records, error) in
                                        
                                        if error == nil {
                                            
                                            print("Third query done!")
                                            
                                            self.records = records!
                                            self.tableView.reloadData()
                                            
                                        }else{
                                            
                                            print("ERROR 3")
                                        }
                                    })
                                }
                                else{
                                    
                                    print("ERROR 2!")
                                }
                                
                            })
                        })
                        
                    }else{
                        
                        print("ERROR!")
                    }
                    
                    
                })
            
            })
    }
    
    override func viewWillAppear(animated: Bool) {
        
       // self.fetchLists()
        self.fetchRecords()
    }
    func fetchLists(){
        
        let privateDB = CKContainer.defaultContainer().privateCloudDatabase
        let query = CKQuery(recordType: "Lists", predicate: NSPredicate(format: "TRUEPREDICATE"))
        query.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        
        privateDB.performQuery(query, inZoneWithID:
        nil) { (records:[CKRecord]?, error:NSError?) in
            
            dispatch_async(dispatch_get_main_queue(), {
            
            
                self.processResponseForQuery(records, error: error)
                
            })
        }
    }
    
    func processResponseForQuery(records: [CKRecord]?, error: NSError?){
        
        if error == nil{
            
            guard records != nil else {
                return
            }
            
            self.records = records!
            self.tableView.reloadData()
        }
    }
    func fetchUserRecordID() -> (){
        
        let defaultContainer = CKContainer.defaultContainer()
        
        defaultContainer.fetchUserRecordIDWithCompletionHandler { (recordID: CKRecordID?, error: NSError?) in
            
            if error != nil {
                
                print("Error at EntryViewController -> fetchUserRecordID")
                print(error.debugDescription)
                
            }else{
                
                guard let userRecordID = recordID else{
                    return
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    self.fetchUserRecord(userRecordID)
                })
            }
        }
    }
    
    func fetchUserRecord(recordID: CKRecordID){
        
        let defaultContainer = CKContainer.defaultContainer()
        let privateDB = defaultContainer.privateCloudDatabase
        
        privateDB.fetchRecordWithID(recordID) { (record: CKRecord?, error: NSError?) in
            
            if error != nil {
                
                print("Error at EntryViewController -> fetchUserRecord")
                print(error.debugDescription)
            }else{
                
                guard let userRecord = record else{
                    return
                }
                
                print(userRecord)
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.records.count
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        cell.accessoryType = .DetailDisclosureButton
        
        let record = records[indexPath.row]
       
        if let name = record.objectForKey("name") as? String{
            
            cell.textLabel?.text = name
            
        }
        else{
            
            cell.textLabel?.text = " - - -"
        }
        
        return cell
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
