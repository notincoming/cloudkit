//
//  AddViewController.swift
//  ICloudTryOut
//
//  Created by Macstorm on 20.09.2016.
//  Copyright © 2016 spot. All rights reserved.
//

import UIKit
import CloudKit

class AddViewController: UIViewController {
    
    let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBAction func cancel(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func save(sender: AnyObject) {
        
        if !(self.nameTextField.text?.isEmpty)!{
            
            /*let privateDB = CKContainer.defaultContainer().privateCloudDatabase
            let record = CKRecord(recordType: "Lists")
            record.setValue(self.nameTextField.text!, forKey: "name")
            
            privateDB.saveRecord(record, completionHandler: {
                
                (_ ,_) in
                
                dispatch_async(dispatch_get_main_queue(), {
                
                    self.navigationController?.popViewControllerAnimated(true)
                })
                
            })
            */
            
            let privateDB = CKContainer.defaultContainer().privateCloudDatabase
            
            let record = CKRecord(recordType: "Protected")
            record.setObject(self.delegate.refList, forKey: "list")
            record.setObject(self.delegate.refPass, forKey: "password")
            record.setObject(self.nameTextField.text!, forKey: "name")
            
            privateDB.saveRecord(record, completionHandler: {
            
                (_, _) in
                dispatch_async(dispatch_get_main_queue(), {
                
                      self.navigationController?.popViewControllerAnimated(true)
                })
              
            })
            
        }
        else{
            
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
